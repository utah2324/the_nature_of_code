﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightRender : MonoBehaviour
{
    private MeshRenderer render;
    public GameObject player;
    public LayerMask layer;

    // Start is called before the first frame update
    void Start()
    {
        render = GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        Active();
    }

    private void Active()
    {
        Ray ray = new Ray(player.transform.position, player.transform.forward);
        RaycastHit hit;

        if(Physics.Raycast(ray, out hit, 10, layer))
        {
            render.enabled = false;
            hit.collider.gameObject.GetComponent<MeshRenderer>().enabled = true;
        } 
    }


}
