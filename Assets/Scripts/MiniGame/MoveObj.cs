﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObj : MonoBehaviour
{   
    
    //Move obj linearly ahihi
    public static IEnumerator MoveAtoB(Transform obj, Vector3 desPos, bool is_active)
    {
        
        float currentTime = Time.time;
        Vector3 startPos = obj.transform.position;

        while (obj.transform.position != desPos)
        {
            yield return new WaitForSeconds(Time.deltaTime);
            obj.transform.position = Vector3.Lerp(startPos, desPos, Time.time - currentTime);
        }
        obj.gameObject.SetActive(is_active);
        //Debug.Log(obj.gameObject.name);
    }
}
