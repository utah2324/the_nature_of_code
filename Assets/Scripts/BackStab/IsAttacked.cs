﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsAttacked : MonoBehaviour
{

    private void Start()
    {
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(Vector3.Dot(transform.up, collision.transform.up) > 0.5f)
        {
            print("BACKSTAB HIT");
            transform.Translate(transform.up * 7);
        }
        else
        {
            print("NO HIT");
            transform.Translate(transform.up * 1);
        }
    }
}
