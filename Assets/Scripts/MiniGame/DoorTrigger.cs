﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorTrigger : MonoBehaviour
{
    private bool is_trigger = true;
    private void OnTriggerEnter(Collider other)
    {
        if (is_trigger)
        {
            GameEvent.instance.OnDoorTriggerEnter();
            is_trigger = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (!is_trigger)
        {
            GameEvent.instance.OnDoorTriggerExit();
            is_trigger = true;
        }
    }
}
